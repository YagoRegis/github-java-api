package com.cursoandroid.examplerecyclerview.desafioandroid.network;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import retrofit2.Retrofit;

import static org.junit.Assert.*;

/**
 * Created by -Yago- on 21/06/2017.
 */
public class GithubApiTest {

    Retrofit retrofit;

    @Before
    public void setUp() throws Exception {
        retrofit = GithubApi.getRetrofit();
    }

    @Test
    public void testGetRetrofitUrl() throws Exception {
        String result = "https://api.github.com/";
        assertEquals(result, retrofit.baseUrl().toString());
    }

    @Test
    public void testGetRetrofitNotNull() throws Exception {
        assertNotNull(retrofit);
    }

}