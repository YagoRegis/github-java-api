package com.cursoandroid.examplerecyclerview.desafioandroid.adapter;

import android.content.Context;
import android.test.AndroidTestCase;
import android.view.View;
import android.view.ViewGroup;

import com.cursoandroid.examplerecyclerview.desafioandroid.models.Owner;
import com.cursoandroid.examplerecyclerview.desafioandroid.models.Repositorie;
import com.cursoandroid.examplerecyclerview.desafioandroid.models.RepositoriesList;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by c00075 on 26/06/2017.
 */
public class RepositoriesAdapterTest {
    private RepositoriesAdapter adapter;
    private RepositoriesList repositoriesList;
    private Repositorie repositorie1;
    private Repositorie repositorie2;
    private Context context;
    private ViewGroup parent;
    private int viewType;

    private Owner owner;

    public RepositoriesAdapterTest() {
        super();
    }

    @Before
    public void setUp() throws Exception {
        ArrayList<Repositorie> arrayList = new ArrayList<>();
        owner = new Owner("test", "test", 1, "test");
        repositorie1 = new Repositorie("test", 10, "test", "www.test.com.br", "test", owner, 10);
        repositorie2 = new Repositorie("test", 15, "test", "www.test.com.br", "test", owner, 20);
        arrayList.add(repositorie1);
        arrayList.add(repositorie2);
        repositoriesList = new RepositoriesList(true, arrayList, 100);

        adapter = new RepositoriesAdapter(arrayList, context);
    }

    @Test
    public void testOnCreateViewHolder() throws Exception {
        RepositoriesAdapter.ViewHolder view = adapter.onCreateViewHolder(parent, viewType);
        assertNull(view);
    }

    @Test
    public void testGetItemCount() throws Exception {
        assertEquals(2, adapter.getItemCount());
    }

}