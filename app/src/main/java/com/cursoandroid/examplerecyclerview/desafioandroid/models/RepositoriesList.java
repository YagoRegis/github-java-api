package com.cursoandroid.examplerecyclerview.desafioandroid.models;

/**
 * Created by -Yago- on 12/06/2017.
 */

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RepositoriesList {

    @SerializedName("total_count")
    @Expose
    private Integer totalCount;
    @SerializedName("incomplete_results")
    @Expose
    private Boolean incompleteResults;
    @SerializedName("items")
    @Expose
    private ArrayList<Repositorie> items = null;

    public RepositoriesList(Boolean incompleteResults, ArrayList<Repositorie> items, Integer totalCount) {
        this.incompleteResults = incompleteResults;
        this.items = items;
        this.totalCount = totalCount;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Boolean getIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(Boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public ArrayList<Repositorie> getItems() {
        return items;
    }

    public void setItems(ArrayList<Repositorie> items) {
        this.items = items;
    }
}
