package com.cursoandroid.examplerecyclerview.desafioandroid.network;

import com.cursoandroid.examplerecyclerview.desafioandroid.models.PullRequest;
import com.cursoandroid.examplerecyclerview.desafioandroid.models.RepositoriesList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by -Yago- on 12/06/2017.
 */

public interface GithubService {

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<RepositoriesList> getRepositoriesList(@Query("page") int page);

    @GET("repos/{login}/{name}/pulls")
    Call<ArrayList<PullRequest>> getPullRequestList(@Path("login") String login, @Path("name")String name);

}
