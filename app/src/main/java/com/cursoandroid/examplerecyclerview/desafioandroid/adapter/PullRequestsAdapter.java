package com.cursoandroid.examplerecyclerview.desafioandroid.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cursoandroid.examplerecyclerview.desafioandroid.R;
import com.cursoandroid.examplerecyclerview.desafioandroid.models.PullRequest;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by -Yago- on 18/06/2017.
 */

public class PullRequestsAdapter extends RecyclerView.Adapter<PullRequestsAdapter.ViewHolder> {
    private ArrayList<PullRequest> pullRequests;
    private Context mContext;

    public PullRequestsAdapter(ArrayList<PullRequest> pullRequests, Context mContext) {
        this.pullRequests = pullRequests;
        this.mContext = mContext;
    }

    @Override
    public PullRequestsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pull_request_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PullRequestsAdapter.ViewHolder holder, int position) {
        Picasso.with(mContext).load(pullRequests.get(position).getUser().getAvatarUrl()).fit().centerCrop()
                .into(holder.iv_user_avatar);
        holder.tv_pr_user.setText(pullRequests.get(position).getUser().getLogin());
        holder.tv_pr_title.setText(pullRequests.get(position).getTitle());
        holder.tv_pr_date.setText(pullRequests.get(position).getCreatedAt());
        holder.tv_pr_body.setText(pullRequests.get(position).getBody());
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_pr_user, tv_pr_title, tv_pr_date, tv_pr_body;
        private ImageView iv_user_avatar;

        public ViewHolder(final View itemView) {
            super(itemView);

            iv_user_avatar = (ImageView) itemView.findViewById(R.id.iv_user_avatar);
            tv_pr_user = (TextView) itemView.findViewById(R.id.tv_pr_user);
            tv_pr_title = (TextView) itemView.findViewById(R.id.tv_pr_title);
            tv_pr_date = (TextView) itemView.findViewById(R.id.tv_pr_date);
            tv_pr_body = (TextView) itemView.findViewById(R.id.tv_pr_body);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = itemView.getContext();
                    String url = pullRequests.get(getAdapterPosition()).getHtmlUrl();
                    Uri webPage = Uri.parse(url);

                    Intent intent = new Intent(Intent.ACTION_VIEW, webPage);

                    if(intent.resolveActivity(context.getPackageManager()) != null) {
                        context.startActivity(intent);
                    }
                }
            });
        }
    }
}
